
brp - (real-time) bitrate pipe
==============================

brp is a small unix tool that behaves like a pipe but with limited bitrate.
It can be used to limit the flow of data or trigger other processes by sending them data.

In addition, brp serves as educational example to demonstrate how C-code can be tested with different test frameworks.
Therefore, multiple test and analysis tools are used.


Features:
---------

- A real-time mode shall use clock_nanosleep for precise timing on PREEMPT-RT systems
- ANSI-C and POSIX compilant


Usage example e.g.:
-------------------

Slow printout:

    ./brp --delay 100 < hdmakefile

Send slow garbage to network host:

    cat /dev/urandom | ./brp --delay=10 | nc somehost someport


Arguments (implemented):
--------------------

    --delay     / -d # delay in ms


Arguments (planned):
--------------------

    --rate=1    / -r # in bit per second <byte>[b,B][K,M] ...
    OR
    --delay     / -d # delay in ms
    --chunk=10B / -c # chunk size
    --buffer    / -b # buffer size
    --timer     / -t # timer that shall be used
    inputfile | - # for stdin


Build:
------

    $ mkdir build
    $ cd build
    $ cmake ../src/
    $ make


Similar projects:
-----------------

- https://github.com/rehno-lindeque/ratelimit-pipe
- https://github.com/rom1v/delay


Building with make
-----------------

    $ cd build
    $ cmake ../src
    $ make

    $ cd build_test
    $ cmake ../test/GoogleTestHippoMocks
    $ make


Eclipse integration
-------------------

    $ cd build
    $ cmake -G"Eclipse CDT4 - Unix Makefiles" ../src
    $ chmod +x brp # why?

    $ cd build_test
    $ cmake -G"Eclipse CDT4 - Unix Makefiles" ../test/GoogleTestHippoMocks
    $ chmod +x googletest_hippomocks # why?

