
/*

echo wheowee | ./brp > brp.txt

*/

#include <stdio.h>
#include <argp.h>
#include <time.h>

static struct argp_option options[] = {
  {"delay",  'd', "delay",      0,  "Delay time in ms" },
  { 0 }
};

struct struct_arguments {
    int delay;
};

//Would be nice within main but we cannot observe / modify the contents there
struct struct_arguments arguments;


static char args_doc[] = " < nice usage info >";

static char doc[] =
  "Argp example #3 -- a program with options and arguments using argp";

#define STRINGIFY(x) #x
#define TOSTRING(x) STRINGIFY(x)

static error_t parse_opt(int key, char *arg, struct argp_state *state)
{
    struct struct_arguments *arguments = state->input;
    char * conversion_rest;

    switch (key)
    {
    case 'd':
        arguments->delay = strtol(arg, &conversion_rest, 10);
        if ( arg == conversion_rest) {
            printf("Error while parsing argument: %s %i\n", arg, strlen(conversion_rest));
            argp_usage (state);
            return -1;
        }

        break;

    case ARGP_KEY_ARG:
        printf("case ARGP_KEY_ARG %i :\n", state->arg_num);
        if (state->arg_num >= 0) {
          printf("Error: Too many arguments.\n");
          argp_usage (state);
          return -1;
      }
      break;

    case ARGP_KEY_END:
        //printf("case ARGP_KEY_END %i :\n", state->arg_num);
/*      if (state->arg_num < 0) {
          printf("Error: Not enough arguments.\n");
          argp_usage (state);
      }*/
          break;

    case ARGP_KEY_ERROR:
          printf("Error: ARGP_KEY_ERROR.\n");

    // Ingnore these:
    case ARGP_KEY_FINI:
    case ARGP_KEY_INIT:
    case ARGP_KEY_NO_ARGS:
    case ARGP_KEY_SUCCESS:
        break;

    default:
        printf("ARGP_ERR_UNKNOWN %X\n", key);
        return ARGP_ERR_UNKNOWN;
    }
    return 0;
}

void print_help() {
    printf("Print da help\n");
}

int sleep_facade( int ms) {
    struct timespec ts;
    ts.tv_sec = 0;
    unsigned long int ns = (unsigned long int)ms * 1000000;
    while (ns >= 1000000000 ) {
        ts.tv_sec ++;
        ns = ns - 1000000000;
    }
    ts.tv_nsec = ns;
    int ret = nanosleep( &ts, NULL);
    return ret;
}

// filecopy: Taken vom Kerninghan & Ritchie
int filecopy( FILE * ifp, FILE * ofp, struct struct_arguments * args) {
    int c;

    while ((c = getc(ifp)) != EOF) {
        putc(c, ofp);
        fflush(ofp);
        sleep_facade( args->delay );
    }
    return 0;
}

static struct argp argp = { options, parse_opt, args_doc, doc };


#ifdef TESTME
int main_orig(int argc, char * argv[]) {
#else
int main(int argc, char * argv[]) {
#endif
    arguments.delay = 0;

/*  printf("Arguments: %i\n", argc);
    for (int i=0; i<argc; i++) {
        printf(" %i %s\n",i, argv[i]);
    } */

    int ret = argp_parse (&argp, argc, argv, ARGP_NO_EXIT, 0, &arguments);

    if (ret == 0) {
        filecopy (stdin, stdout, &arguments );
    }

    return ret;
}

