
/*
Build Info:

cd ~/Pile/CodeLand/BitRatePipe/build_test$
rm -rf *
cmake ../test/GoogleTestHippoMocks/
make
*/


#include <string>
#include <argp.h>
#include <time.h>

#include "gtest/gtest.h"
#include "hippomocks.h"

using HippoMocks::byRef;

extern "C" {

void print_help();
int main_orig(int argc, char * argv[]);
int filecopy( FILE * ifp, FILE * ofp, struct struct_arguments * args);
int sleep_facade(int ms);

struct struct_arguments {
    int delay;
};

extern struct_arguments arguments;

}

class BrpTest : public ::testing::Test {
 protected:

  BrpTest() {
  }

  ~BrpTest() override {
  }

  void SetUp() override {
  }

  void TearDown() override {
  }

};

int return_one() {
    return 1;
}

//infrastructure to incercept values given to nanosleep
struct timespec global_ts_storage;
int nanosleep_fake ( struct timespec * tv, struct timespec * rem) {
    global_ts_storage.tv_sec = tv->tv_sec;
    global_ts_storage.tv_nsec = tv->tv_nsec;
}

// Same interface as fred
/*
int injectBuffer( void * f) {
    printf("Injecting into Buffer\n");
    char* injection = const_cast<char*>("mockmockmock");
    memcpy(b,injection,10);
    return 0;
}
*/

TEST_F(BrpTest, Pass) {
    EXPECT_EQ( 1,1);
}

TEST_F(BrpTest, TestBaseFunctionality) {
    MockRepository mocks;
    mocks.OnCallFunc( return_one ).Return( 2 );
    EXPECT_EQ( return_one(), 2);
}

TEST_F( BrpTest, Main) {
    int argc = 3;

    // TODO Ugly
    // warning: ISO C++ forbids converting a string constant to ‘char*’ [-Wpedantic]
    // Option : static_cast<char*>("one")
    std::string one ("./brp");
    std::string two ("-d");
    std::string three ("200");
    char *argv[] = {
        const_cast<char*>(one.c_str()),
        const_cast<char*>(two.c_str()),
        const_cast<char*>(three.c_str())
        };

    // Temporarily simulate EOF via stdin
    MockRepository mocks;
    mocks.OnCallFunc( getc ).Return( EOF );

    int ret = main_orig( argc, argv );
    EXPECT_EQ( arguments.delay, 200);
    EXPECT_EQ( ret, 0);
}

// create a c-string from a string
inline char * cs (std::string s){
	//Todo this is a memory leak!
	char * r = new char[s.length() + 1];
	std::strcpy(r, s.c_str());
    return r;
}

TEST_F(BrpTest, Help) {
    print_help();
}

TEST_F(BrpTest, Filecopy_multiple_bytes) {
    struct struct_arguments arg;
    arg.delay = 100;
    MockRepository mocks;

    // ExpectCallFunc needs to be in the order of calls !
    mocks.ExpectCallFunc( getc ).Return( '1' );
    mocks.ExpectCallFunc( getc ).Return( '2' );
    mocks.ExpectCallFunc( getc ).Return( '3' );
    mocks.ExpectCallFunc( getc ).Return( EOF );

    mocks.OnCallFunc( sleep_facade ).Return(0);
    mocks.OnCallFunc( putc ).Return(0);
    int ret = filecopy(NULL, NULL, &arg);
    EXPECT_EQ( ret, 0);
}

TEST_F(BrpTest, Filecopy) {
    struct struct_arguments arg;
    arg.delay = 100;
    MockRepository mocks;

    mocks.ExpectCallFunc( getc ).Return( '1' );
    mocks.ExpectCallFunc( putc ).Return(0);
    mocks.ExpectCallFunc( fflush ).Return(0);
    mocks.ExpectCallFunc( sleep_facade ).Return(0);
    mocks.ExpectCallFunc( getc ).Return( EOF );

    int ret = filecopy(NULL, NULL, &arg);
    EXPECT_EQ( ret, 0);
}

TEST_F(BrpTest, Filecopy_200ms) {
    struct struct_arguments arg;
    arg.delay = 200;
    MockRepository mocks;

    mocks.OnCallFunc( fflush ).Return(0);
    mocks.OnCallFunc( sleep_facade ).With(200).Return(0);
    mocks.OnCallFunc( getc ).Return( EOF );

    mocks.ExpectCallFunc( getc ).Return( '1' );
    mocks.ExpectCallFunc( putc ).Return(0);

    int ret = filecopy(NULL, NULL, &arg);
    EXPECT_EQ( ret, 0);
}

TEST_F(BrpTest, sleep_facade) {
    MockRepository mocks;
    mocks.ExpectCallFunc( nanosleep ).Return( 0 );
    int ret = sleep_facade( 100 );
    EXPECT_EQ( ret, 0);
}

TEST_F(BrpTest, sleep_facade_200) {
    MockRepository mocks;
    mocks.ExpectCallFunc( nanosleep ).Do(nanosleep_fake).Return( 0 );
    int ret = sleep_facade( 200 );
    EXPECT_EQ( global_ts_storage.tv_sec, 0);
    EXPECT_EQ( global_ts_storage.tv_nsec, 200 * 1000 * 1000);
    EXPECT_EQ( ret, 0);
}

TEST_F(BrpTest, sleep_facade_1000) {
    MockRepository mocks;
    mocks.ExpectCallFunc( nanosleep ).Do(nanosleep_fake).Return( 0 );
    int ret = sleep_facade( 1000 );
    EXPECT_EQ( global_ts_storage.tv_sec, 1);
    EXPECT_EQ( global_ts_storage.tv_nsec, 0);
    EXPECT_EQ( ret, 0);
}

TEST_F(BrpTest, sleep_facade_2999) {
    MockRepository mocks;
    mocks.ExpectCallFunc( nanosleep ).Do(nanosleep_fake).Return( 0 );
    int ret = sleep_facade( 2999 );
    EXPECT_EQ( global_ts_storage.tv_sec, 2);
    EXPECT_EQ( global_ts_storage.tv_nsec, 999000000);
    EXPECT_EQ( ret, 0);
}

TEST_F(BrpTest, sleep_facade_29999) {
    MockRepository mocks;
    mocks.ExpectCallFunc( nanosleep ).Do(nanosleep_fake).Return( 0 );
    int ret = sleep_facade( 29999 );
    EXPECT_EQ( global_ts_storage.tv_sec, 29);
    EXPECT_EQ( global_ts_storage.tv_nsec, 999000000);
    EXPECT_EQ( ret, 0);
}

// Tests for the main entry and argument parsing

TEST_F(BrpTest, Sleep_delay_100ms) {
    int argc = 3;
    char *argv[] = { cs("./brp"), cs("-d"),  cs("100") };

    MockRepository mocks;
     //Does not work: /byRef(arguments));
    mocks.ExpectCallFunc( filecopy ).With(_,_, &arguments).Return(0);
    int ret = main_orig( argc, argv );
    EXPECT_EQ( arguments.delay, 100);
    EXPECT_EQ( ret, 0);
}

TEST_F(BrpTest, Sleep_delay_1ms) {
    int argc = 3;
    char *argv[] = { cs("./brp"), cs("-d"),  cs("1") };
    MockRepository mocks;
    mocks.ExpectCallFunc( filecopy ).With(_,_, &arguments).Return(0);
    int ret = main_orig( argc, argv );
    EXPECT_EQ( arguments.delay, 1);
    EXPECT_EQ( ret, 0);
}

TEST_F(BrpTest, Sleep_delay_1ms_long) {
    int argc = 3;
    char *argv[] = { cs("./brp"), cs("--delay"),  cs("1") };
    MockRepository mocks;
    mocks.ExpectCallFunc( filecopy ).With(_,_, &arguments).Return(0);
    int ret = main_orig( argc, argv );
    EXPECT_EQ( arguments.delay, 1);
    EXPECT_EQ( ret, 0);
}

TEST_F(BrpTest, Sleep_delay_1ms_long_equal_sign) {
    int argc = 2;
    char *argv[] = { cs("./brp"), cs("--delay=1")};
    MockRepository mocks;
    mocks.ExpectCallFunc( filecopy ).With(_,_, &arguments).Return(0);
    int ret = main_orig( argc, argv );
    EXPECT_EQ( arguments.delay, 1);
    EXPECT_EQ( ret, 0);
}

TEST_F(BrpTest, Sleep_delay_invalidms) {
    int argc = 3;
    char *argv[] = { cs("./brp"), cs("-d"),  cs("invalid") };
    MockRepository mocks;
    //mocks.NeverCallFunc( filecopy );
    mocks.ExpectCallFunc( argp_usage);
    int ret = main_orig( argc, argv );
    EXPECT_EQ( arguments.delay, 0); //unchanged
    EXPECT_NE( ret, 0);
}

TEST_F(BrpTest, Parse_too_many_args) {
    int argc = 2;
    char *argv[] = { cs("./brp"), cs("one")}; //, cs("two"),  cs("three") };

    MockRepository mocks;
    mocks.OnCallFunc( filecopy ).Return(0);

    // Hint: argp_usage calls exit !
    // We cannot mock exit or exit_group in hippomocks as they do not return
    // It also seems like we also can't mock __argp_state_help (libc: argp-help.c)
    // mocks.ExpectCallFunc(exit);//.With(64);
    // https://github.com/dascandy/hippomocks/issues/40
    mocks.OnCallFunc( argp_usage);
    int ret = main_orig( argc, argv );
    EXPECT_NE( ret, 0);
}

TEST_F(BrpTest, Parse_no_args) {
    int argc = 1;
    char *argv[] = { cs("./brp") };
    MockRepository mocks;
    mocks.ExpectCallFunc(filecopy ).Return(0);
    int ret = main_orig( argc, argv );
    EXPECT_EQ( ret, 0);
}

TEST_F(BrpTest, Main_call_argp_parse) {
    int argc = 1;
    char *argv[] = { cs("./brp") };

    MockRepository mocks;
    mocks.OnCallFunc( filecopy ).Return(0);
    mocks.ExpectCallFunc(argp_parse).Return(0);
    int ret = main_orig( argc, argv );
    EXPECT_EQ( ret, 0);
}

int main(int argc, char **argv) {
  ::testing::InitGoogleTest(&argc, argv);
  return RUN_ALL_TESTS();
}
